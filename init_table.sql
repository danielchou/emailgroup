USE [ITPortal]
GO

/****** Object:  Table [dbo].[EmailGroup_Emails]    Script Date: 2014/5/29 �U�� 07:20:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailGroup_Emails](
	[SN] [int] IDENTITY(1,1) NOT NULL,
	[GroupEmail] [nvarchar](250) NULL,
	[GmailAddress] [nvarchar](250) NULL,
	[IsUserVisible] [nchar](1) NULL,
	[UpdateTime] [smalldatetime] NULL,
 CONSTRAINT [PK_EmailGroup_Emails] PRIMARY KEY CLUSTERED 
(
	[SN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [ITPortal]
GO

/****** Object:  Table [dbo].[EmailGroup_Mapping]    Script Date: 2014/5/29 �U�� 07:20:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailGroup_Mapping](
	[ADGroup] [nvarchar](80) NOT NULL,
	[GmailGroup] [nvarchar](150) NOT NULL,
	[GmailAddress] [nvarchar](250) NULL,
	[IsUserVisible] [nchar](1) NULL,
	[Updatetime] [smalldatetime] NULL,
 CONSTRAINT [PK_EmailGroup_GroupMapping] PRIMARY KEY CLUSTERED 
(
	[ADGroup] ASC,
	[GmailGroup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


