'use strict';

/* Services */

var emailServices = angular.module('emailServices', ['ngResource']);

emailServices.factory('EmailServ', ['$resource', function ($resource) {
      return $resource(host + '/Query/:action/:emailaddress', { emailaddress: "@emailaddress" }, {
          query: { method: 'GET', isArray: true, params: { action:"Email", emailaddress: "" } },
          get: { method: "GET", isArray: true, params:{action:"Email"} },
          getGroup: { method: "GET", isArray: true, params: { action: "EmailGG"} },
          save :{ method :"POST", params: { action:"SaveEmail"} },
          deleteMail: { method: "POST", params: { action: "DeleteEmail"} },
          showLog: { method: "POST", isArray: true, params: { action: "ShowLogJson"} }
        });
  }]);
